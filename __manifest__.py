{
    'name': 'Resumen de movimientos',
    'version': '14.0.1.0.0',
    'category': 'stock',
    'summery': 'Resumen de movimientos',
    'author': 'Fixdoo',
    'depends': ['stock', 'sale_management'],
    'data': [
        'security/ir.model.access.csv',
        'views/resumen_movimientos_views.xml',
    ],
    'license': "LGPL-3",
    'installable': True,
    'application': False,
}
