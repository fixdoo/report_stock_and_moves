from odoo import api, fields, models, tools
from odoo.tools.safe_eval import pytz


class ResumenMovimientos(models.Model):
    _name = "resumen.movimientos"
    _auto = False
    _order = "product_id desc"

    product_id = fields.Many2one("product.product")
    default_code = fields.Char("")
    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse')
    fecha_movimiento = fields.Datetime("Fecha movimiento")
    origin = fields.Char("Origen")
    picking_type_id = fields.Many2one('stock.picking.type')
    qty_done = fields.Float()
    price_unit = fields.Float()
    price_total = fields.Float()
    costo_unitario = fields.Float(compute="_compute_costos")
    costo_total = fields.Float(compute="_compute_costos")

    def _compute_costos(self):
        for record in self:
            record.costo_unitario = record.product_id.standard_price
            record.costo_total = record.costo_unitario * record.qty_done

    def _get_main_request(self):
        request = """
            CREATE or REPLACE VIEW %s AS (
                SELECT 
                    row_number() OVER (PARTITION BY true) as id,
                    sm.product_id AS product_id,
                    pp.default_code AS default_code,
                    sm.warehouse_id AS warehouse_id,
                    sm.date AS fecha_movimiento,
                    sm.origin AS origin,
                    sm.picking_type_id AS picking_type_id,
                    sml.qty_done AS qty_done,
                    sol.price_unit As price_unit,
                    sol.price_total AS price_total
                FROM 
                    stock_move AS sm
                INNER JOIN 
                    stock_move_line AS sml
                ON
                    sml.move_id = sm.id
                INNER JOIN
                    product_product AS pp
                ON
                    sm.product_id = pp.id
                LEFT JOIN
                    sale_order_line AS sol
                ON
                    sm.sale_line_id = sol.id
            )
        """ % (self._table,)
        return request

    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute(self._get_main_request())
